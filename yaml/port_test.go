package porter

import (
	"os"
	"testing"
)

type P struct {
	Name string
}

var filename = os.TempDir() + "/test.yaml"

var o = &P{"Porter name"}

func TestYaml(t *testing.T) {
	pj := New(filename)
	if err := pj.Export(o); err != nil {
		t.Errorf("Export Error: %s", err.Error())
	}
	oo := new(P)
	if err := pj.Import(oo); err != nil {
		t.Errorf("Import Error: %s", err.Error())
	}
	if oo.Name != o.Name {
		t.Errorf("Objects don't match:\n Import: %v\n Export: %v\n", oo, o)
	}
	os.Remove(filename)
}
