// Import/Export data from/to a Yaml File
package porter

import (
	"io/ioutil"

	"bitbucket.org/gotamer/porter"
	"gopkg.in/yaml.v2"
)

type PortYaml struct {
	file string
}

// New returns the correct porter
func New(file string) porter.Porter {
	return &PortYaml{file}
}

// Import loads your struct from the given json file
func (p *PortYaml) Import(o interface{}) error {
	b, err := ioutil.ReadFile(p.file)
	if err == nil {
		err = yaml.Unmarshal(b, o)
	}
	return err
}

// Export will save your struct to the given file
func (p *PortYaml) Export(o interface{}) error {
	j, err := yaml.Marshal(&o)
	if err == nil {
		err = ioutil.WriteFile(p.file, j, 0660)
	}
	return err
}
