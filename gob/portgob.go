// Import/Export data to a Gob File
package porter

import (
	"io/ioutil"

	"bitbucket.org/gotamer/porter"
	"bitbucket.org/gotamer/sbs"
)

type PortGob struct {
	file string
}

// New returns the correct porter
func New(file string) porter.Porter {
	return &PortGob{file}
}

// Import loads your struct from the given file
func (p *PortGob) Import(o interface{}) error {
	g, err := ioutil.ReadFile(p.file)
	if err == nil {
		err = sbs.Dec(o, g)
	}
	return err
}

// Export will save your struct to the given file
func (p *PortGob) Export(o interface{}) error {
	bs, err := sbs.Enc(o)
	if err == nil {
		err = ioutil.WriteFile(p.file, bs, 0660)
	}
	return err
}
