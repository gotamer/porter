package porter

type Porter interface {
	Import(o interface{}) error
	Export(o interface{}) error
}
