
Import/Export data from/to a File
---------------------------------

#### Implemented file types JSON, GOB, TOML, YAML



[![GoDoc Json](https://godoc.org/bitbucket.org/gotamer/porter/json?status.svg)](https://godoc.org/bitbucket.org/gotamer/porter/json) Porter JSON

[![GoDoc Gob](https://godoc.org/bitbucket.org/gotamer/porter/gob?status.svg)](https://godoc.org/bitbucket.org/gotamer/porter/gob) Porter Gob

[![GoDoc Yaml](https://godoc.org/bitbucket.org/gotamer/porter/yaml?status.svg)](https://godoc.org/bitbucket.org/gotamer/porter/yaml) Porter Yaml

[![GoDoc Toml](https://godoc.org/bitbucket.org/gotamer/porter/toml?status.svg)](https://godoc.org/bitbucket.org/gotamer/porter/toml) Porter Toml



--------------------


The MIT License (MIT)
=====================

Copyright © 2013 Dennis T Kaplan <http://www.robotamer.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE
