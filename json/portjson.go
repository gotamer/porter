// Import/Export data from/to a Json File
package porter

import (
	"encoding/json"
	"io/ioutil"

	"bitbucket.org/gotamer/porter"
)

type PortJson struct {
	file string
}

// New returns the correct porter
func New(file string) porter.Porter {
	return &PortJson{file}
}

// Import loads your struct from the given json file
func (p *PortJson) Import(o interface{}) error {
	b, err := ioutil.ReadFile(p.file)
	if err == nil {
		err = json.Unmarshal(b, &o)
	}
	return err
}

// Export will save your struct to the given file
func (p *PortJson) Export(o interface{}) error {
	j, err := json.MarshalIndent(&o, "", "\t")
	if err == nil {
		err = ioutil.WriteFile(p.file, j, 0660)
	}
	return err
}
