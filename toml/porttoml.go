// Import/Export data from/to a Toml File
package porter

import (
	"bytes"
	"io/ioutil"

	"bitbucket.org/gotamer/porter"
	"github.com/BurntSushi/toml"
)

type PortToml struct {
	file string
}

// New returns the correct porter
func New(file string) porter.Porter {
	return &PortToml{file}
}

// Import loads your struct from the given json file
func (p *PortToml) Import(o interface{}) error {
	b, err := ioutil.ReadFile(p.file)
	if err == nil {
		err = toml.Unmarshal(b, o)
	}
	return err
}

// Export will save your struct to the given file
func (p *PortToml) Export(o interface{}) error {
	buf := new(bytes.Buffer)
	err := toml.NewEncoder(buf).Encode(o)
	if err == nil {
		err = ioutil.WriteFile(p.file, buf.Bytes(), 0660)
	}
	return err
}
